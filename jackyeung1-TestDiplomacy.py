from unittest import main, TestCase
from Diplomacy import diplomacy_solve
from io import StringIO

class TestDiplomacy(TestCase):

    def test_solve1(self):
        # Test a case where the defender has more support than the attacker
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Support A')
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), 'A Madrid\nB [dead]\nC London\n')

    def test_solve2(self):
        # Test a corner case where no support and expecting all to be dead
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid')
        w = StringIO()
        check_value = diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 
            'A [dead]\nB [dead]\nC [dead]\n'
        )

    def test_solve3(self):
        # Test a corner case where support is equal for attackers
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A')
        w = StringIO()
        check_value = diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 
            'A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n'
        )

    def test_solve4(self):
        # Test a case where the attacker has more support than the defender
        r = StringIO('A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Hold')
        w = StringIO()
        check_value = diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), 
            'A [dead]\nB Madrid\nC [dead]\nD Paris\nE Austin\n'
        )


if __name__ == '__main__': # pragma: no cover
    main()
    
    
    
""" #pragma: no cover
$ coverage run --branch TestDiplomacy.py >  TestDiplomacy.out 2>&1

$ cat TestDiplomacy.out
....
----------------------------------------------------------------------
Ran 4 tests in 0.001s

OK

$ coverage report -m >> TestDiplomacy.out

$ cat TestDiplomacy.out
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          38      0     28      0   100%
TestDiplomacy.py      24      0      0      0   100%
--------------------------------------------------------------
TOTAL                 62      0     28      0   100%
"""