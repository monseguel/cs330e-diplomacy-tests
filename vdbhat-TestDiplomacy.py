# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_solve

# -----------
# TestDiplomacy
# -----------


class TestDiplomacy (TestCase):
    # -----
    # solve
    # -----

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve_4(self):
        r = StringIO("A Madrid Move Vienna\nB Berlin Support D\nC Barcelona Move Vienna\nD Vienna Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Berlin\nC [dead]\nD Vienna\n")

    def test_solve_5(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC Vienna Move Madrid\nD London Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Barcelona\nC [dead]\nD London\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move London\nC London Support A\nD Vienna Move Prague\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\nB [dead]\nC [dead]\nD Prague\n")
    
# ----
# main
# ----

if __name__ == "__main__":
    main() # pragma: no cover


