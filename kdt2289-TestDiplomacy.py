from io import StringIO
from unittest import main,TestCase
from Diplomacy import diplomacy_solve

class TestDiplomacy (TestCase):

    def test_solve_1(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\n" )

    def test_solve_2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\n")

    def test_solve_3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Move Madrid\nD Paris Support B\nE Austin Support A")
        w = StringIO()
        diplomacy_solve(r,w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD Paris\nE Austin\n" )
    
    def test_solve_4(self):
        r = StringIO("")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "")
    
    def test_solve_5(self):
        r = StringIO("A Madrid Support B\nB Barcelona Support A\nC London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\n")

    def test_solve_6(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\n") 
    
    def test_solve_7(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support C\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\n")
    
    def test_solve_8(self):
        r = StringIO("A Madrid Move Barcelona\nB Barcelona Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB [dead]\n")
    
    def test_solve_9(self):
        r = StringIO("A Madrid Support B\nB Barcelona Support C\nC London Support A\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\n")
    
    def test_solve_10(self):
        r = StringIO("A Madrid Hold\nB Barcelona Hold\nC London Support A\nD Paris Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD [dead]\n")
    
    def test_solve_11(self):
        r = StringIO("A Madrid Hold\nB Barcelona Support A\nC London Support A\nD Paris Move Madrid\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(w.getvalue(), "A Madrid\nB Barcelona\nC London\nD [dead]\n")

# ----
# main
# ----

if __name__ == "__main__":
    main()

""" #pragma: no cover
$ coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


$ cat TestCollatz.out
...........
----------------------------------------------------------------------
Ran 11 tests in 0.001s

OK


$ coverage report -m                   >> TestDiplomacy.out



$ cat TestDiplomacy.out
.......
----------------------------------------------------------------------
Ran 11 tests in 0.000s
OK
Name               Stmts   Miss Branch BrPart  Cover   Missing
--------------------------------------------------------------
Diplomacy.py          71      0     42      0   100%
TestDiplomacy.py      61      0      0      0   100%
--------------------------------------------------------------
"""