#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3/reference/simple_stmts.html#grammar-token-assert-stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Diplomacy import diplomacy_read, diplomacy_eval, diplomacy_solve

# Rename the test class to TestDiplomacy
class TestDiplomacy(TestCase):
    # ----
    # read
    # ----

    def test_read_hold(self):
        s = "A Madrid Hold\n"
        actions = diplomacy_read(s)
        self.assertEqual(actions, ["A Madrid Hold"])

    def test_read_move(self):
        s = "A Madrid Move London\n"
        actions = diplomacy_read(s)
        self.assertEqual(actions, ["A Madrid Move London"])

    def test_read_support(self):
        s = "A Madrid Support B\n"
        actions = diplomacy_read(s)
        self.assertEqual(actions, ["A Madrid Support B"])

    def test_read_empty(self):
        s = ""
        self.assertRaises(AssertionError, diplomacy_read, s)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        actions = ["A Madrid Hold"]
        result = diplomacy_eval(actions)
        self.assertEqual(result, ["A Madrid"])

    def test_eval_2(self):
        actions = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B"]
        result = diplomacy_eval(actions)
        self.assertEqual(result, ["A [dead]", "B Madrid", "C London"])

    def test_eval_3(self):
        actions = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Move Madrid", "D Paris Support B", "E Austin Support A"]
        result = diplomacy_eval(actions)
        self.assertEqual(result, ["A [dead]", "B [dead]", "C [dead]", "D Paris", "E Austin"])

    def test_eval_4(self):
        actions = ["A Madrid Hold", "B Barcelona Move Madrid"]
        result = diplomacy_eval(actions)
        self.assertEqual(result, ["A [dead]", "B [dead]"])

    def test_eval_5(self): # need to fix
        actions = ["A Madrid Hold", "B Barcelona Move Madrid", "C London Support B", "D Austin Move London"]
        result = diplomacy_eval(actions)
        self.assertEqual(result, ["A [dead]", "B [dead]", "C [dead]", "D [dead]"])   

    # -----
    # solve
    # ----

    def test_solve1(self):
        r = StringIO("A Madrid Hold\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A Madrid\n")

    def test_solve2(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB Madrid\nC London\n")
    
    def test_solve3(self):
        r = StringIO("A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\nD Austin Move London\n")
        w = StringIO()
        diplomacy_solve(r, w)
        self.assertEqual(
            w.getvalue(), "A [dead]\nB [dead]\nC [dead]\nD [dead]\n")

if __name__ == "__main__":
    main()

""" #pragma: no cover
"""
