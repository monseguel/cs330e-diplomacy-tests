from unittest import main, TestCase
from io import StringIO
from Diplomacy import diplomacy_solve, diplomacy_read, diplomacy_print, diplomacy_run


# -----------
# TestDiplomacy
# -----------


class TestDiplomacy(TestCase):
    # ----
    # read
    # ----
    def test_read(self):
        a = diplomacy_read(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B\n")
        o = {"A": {"c": "Madrid", "a": "Hold", "s": 0, "t": None},
             "B": {"c": "Barcelona", "a": "Move", "s": 0, "t": "Madrid"},
             "C": {"c": "London", "a": "Support", "s": 0, "t": "B"}}
        self.assertEqual(a, o)

    # ----
    # solve
    # ----
    def test_solve1(self):
        a = {"A": {"c": "Madrid", "a": "Hold", "s": 0, "t": None},
             "B": {"c": "Barcelona", "a": "Move", "s": 0, "t": "Madrid"},
             "C": {"c": "London", "a": "Support", "s": 0, "t": "B"}}
        o = diplomacy_solve(a)
        self.assertEqual(o, {"A": "[dead]", "B": "Madrid", "C": "London"})

    def test_solve2(self):
        a = {"A": {"c": "Madrid", "a": "Hold", "s": 0, "t": None},
             "B": {"c": "Barcelona", "a": "Move", "s": 0, "t": "Madrid"},
             "C": {"c": "London", "a": "Move", "s": 0, "t": "Madrid"},
             "D": {"c": "Paris", "a": "Support", "s": 0, "t": "B"},
             "E": {"c": "Austin", "a": "Support", "s": 0, "t": "A"}}
        o = diplomacy_solve(a)
        self.assertEqual(
            o, {"A": "[dead]", "B": "[dead]", "C": "[dead]", "D": "Paris", "E": "Austin"})

    def test_solve3(self):
        a = {"A": {"c": "Madrid", "a": "Hold", "s": 0, "t": None},
             "B": {"c": "Barcelona", "a": "Move", "s": 0, "t": "Madrid"},
             "C": {"c": "London", "a": "Support", "s": 0, "t": "B"},
             "D": {"c": "Austin", "a": "Move", "s": 0, "t": "London"}}
        o = diplomacy_solve(a)
        self.assertEqual(
            o, {"A": "[dead]", "B": "[dead]", "C": "[dead]", "D": "[dead]"})

    # ----
    # print
    # ----
    def test_print(self):
        w = StringIO()
        diplomacy_print(w, {"A": "[dead]", "B": "Madrid", "C": "London"})
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")

    # ----
    # run
    # ----
    def test_run(self):
        r = StringIO(
            "A Madrid Hold\nB Barcelona Move Madrid\nC London Support B")
        w = StringIO()
        diplomacy_run(r, w)
        self.assertEqual(w.getvalue(), "A [dead]\nB Madrid\nC London\n")


# ----
# main
# ----

if __name__ == "__main__":
    main()
